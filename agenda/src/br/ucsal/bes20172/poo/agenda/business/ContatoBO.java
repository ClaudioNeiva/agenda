package br.ucsal.bes20172.poo.agenda.business;

import java.util.Collections;
import java.util.List;

import br.ucsal.bes20172.poo.agenda.entity.Contato;
import br.ucsal.bes20172.poo.agenda.exceptions.NomeNaoInformadoException;
import br.ucsal.bes20172.poo.agenda.exceptions.TelefoneInvalidoException;
import br.ucsal.bes20172.poo.agenda.persistence.ContatoDAO;

//BO = Business Object
public class ContatoBO {

	public static void cadastrar(Contato contato) throws NomeNaoInformadoException, TelefoneInvalidoException {
		validar(contato);
		ContatoDAO.inserir(contato);
	}

	/**
	 * Retorna todos os contatos presentes na base de dados, ordenado por nome.
	 * 
	 * @return contatos ordenados por nome.
	 */
	public static List<Contato> obterTodos() {
		List<Contato> contatos = ContatoDAO.obterTodos();
		Collections.sort(contatos);
		return contatos;
	}

	public static List<Contato> pesquisar(String nome) {
		return ContatoDAO.pesquisar(nome);
	}

	private static void validar(Contato contato) throws NomeNaoInformadoException, TelefoneInvalidoException {
		if (contato.getNome().trim().isEmpty()) {
			throw new NomeNaoInformadoException();
		}
		if (contato.getTelefone().trim().length() < 8) {
			throw new TelefoneInvalidoException();
		}
	}

}
