package br.ucsal.bes20172.poo.agenda.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20172.poo.agenda.entity.Contato;

//DAO = Data Access Object
public class ContatoDAO {

	private static List<Contato> contatos = new ArrayList<>();

	public static void inserir(Contato contato) {
		contatos.add(contato);
	}

	public static List<Contato> obterTodos() {
		return contatos;
	}

	public static List<Contato> pesquisar(String nome) {
		List<Contato> resultado = new ArrayList<>();
		for (Contato contato : contatos) {
			if (contato.getNome().equalsIgnoreCase(nome)) {
				resultado.add(contato);
			}
		}
		return resultado;
	}

}
