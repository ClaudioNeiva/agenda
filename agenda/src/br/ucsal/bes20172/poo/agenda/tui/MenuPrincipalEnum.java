package br.ucsal.bes20172.poo.agenda.tui;

public enum MenuPrincipalEnum {
	
	CADASTRAR(1, "Cadastrar"), LISTAR(2, "Listar"), PESQUISAR(3, "Pesquisar"), SAIR(9, "Sair");

	private Integer codigo;
	private String descricao;

	private MenuPrincipalEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public static MenuPrincipalEnum valueOfCodigo(Integer codigo) {
		for(MenuPrincipalEnum item: values()){
			if(item.getCodigo().equals(codigo)){
				return item;
			}
		}
		throw new IllegalArgumentException();
	}

}
