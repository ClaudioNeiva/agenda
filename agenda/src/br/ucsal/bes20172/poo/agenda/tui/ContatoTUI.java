package br.ucsal.bes20172.poo.agenda.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20172.poo.agenda.business.ContatoBO;
import br.ucsal.bes20172.poo.agenda.entity.Contato;
import br.ucsal.bes20172.poo.agenda.exceptions.NomeNaoInformadoException;
import br.ucsal.bes20172.poo.agenda.exceptions.TelefoneInvalidoException;

public class ContatoTUI {

	private static Scanner scanner = new Scanner(System.in);

	public static void cadastrar() {
		System.out.println("******* CADASTRAR CONTATO *******");

		System.out.println("Informe:");

		System.out.print("Nome:");
		String nome = scanner.nextLine();

		System.out.print("Telefone:");
		String telefone = scanner.nextLine();

		Contato contato = new Contato(nome, telefone);

		try {
			ContatoBO.cadastrar(contato);
			System.out.println("Contato cadastrado com sucesso.");
		} catch (NomeNaoInformadoException e) {
			System.out.println("Nome do contato n�o informado.");
		} catch (TelefoneInvalidoException e) {
			System.out.println("Telefone deve ter no m�nimo 8 d�gitos.");
		} catch (Exception e) {
			System.out.println("Erro geral.");
		}

	}

	public static void listar() {
		List<Contato> contatos = ContatoBO.obterTodos();
		exibirContatos(contatos);
	}

	public static void pesquisar() {
		System.out.println("******* PESQUISAR CONTATO *******");
		System.out.print("Informe o nome do contato:");
		String nome = scanner.nextLine();
		List<Contato> contatos = ContatoBO.pesquisar(nome);
		exibirContatos(contatos);
	}

	private static void exibirContatos(List<Contato> contatos) {
		if (contatos.isEmpty()) {
			System.out.println("Nenhum contato encontrado.");
		} else {
			System.out.println("Contatos:");
			for (Contato contato : contatos) {
				System.out.println(contato.getNome() + " - " + contato.getTelefone());
			}
		}
	}

}
