package br.ucsal.bes20172.poo.agenda.tui;

import java.util.Scanner;

//TUI = Text User Interface
public class AgendaTUI {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		MenuPrincipalEnum opcao;

		do {
			exibirOpcoes();
			opcao = obterOpcaoDesejada();
			executar(opcao);
		} while (!opcao.equals(MenuPrincipalEnum.SAIR));
	}

	private static void exibirOpcoes() {
		for (MenuPrincipalEnum item : MenuPrincipalEnum.values()) {
			System.out.println(item.getCodigo() + " - " + item.getDescricao());
		}
	}

	private static MenuPrincipalEnum obterOpcaoDesejada() {
		System.out.println("Informe a op��o desejada:");
		Integer opcao = scanner.nextInt();
		scanner.nextLine();
		return MenuPrincipalEnum.valueOfCodigo(opcao);
	}

	private static void executar(MenuPrincipalEnum opcao) {
		switch (opcao) {
		case CADASTRAR:
			ContatoTUI.cadastrar();
			break;
		case LISTAR:
			ContatoTUI.listar();
			break;
		case PESQUISAR:
			ContatoTUI.pesquisar();
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		default:
			System.out.println("Op��o inv�lida!");
			break;
		}
	}

}
