package br.ucsal.bes20172.poo.aula10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Ordenacao2 {

	public static void main(String[] args) {
		List<Pessoa> pessoas = new ArrayList<>();

		pessoas.add(new Pessoa("claudio", "rua x", 1973));
		pessoas.add(new Pessoa("maria", "rua y", 1975));
		pessoas.add(new Pessoa("pedro", "rua y", 1973));
		pessoas.add(new Pessoa("joaquim", "rua x", 1999));
		pessoas.add(new Pessoa("manuela", "rua x", 1998));
		pessoas.add(new Pessoa("antonio", "rua z", 1999));
		pessoas.add(new Pessoa("clara", "rua x", 1973));

		exibir("Pessoas, conforme informado:", pessoas);

		// Ordenar as pessoas, por anoNascimento (ordem crescente)
		Collections.sort(pessoas);
		exibir("Pessoas, ordenadas por anoNascimento(crescente):", pessoas);

		// Ordenar as pessoas, por nome (ordem crescente)
		// N�o � dessa forma que geralmente vc vai encontrar a solu��o na
		// comunidade, mas como essa forma � mais did�tica, utilizaremos ela POR
		// ENQUANTO.
		ComparadorPessoaPorNome comparadorPessoaPorNome = new ComparadorPessoaPorNome();
		Collections.sort(pessoas, comparadorPessoaPorNome);
		exibir("Pessoas, ordenadas por nome(crescente):", pessoas);

		// Ordenar as pessoas, por endereco (ordem crescente)
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				return o1.getEndereco().compareTo(o2.getEndereco());
			}
		});
		exibir("Pessoas, ordenadas por endere�o(crescente):", pessoas);

	}

	private static void exibir(String mensagem, List<Pessoa> pessoas) {
		System.out.println("\n" + mensagem);
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}
	}

}
