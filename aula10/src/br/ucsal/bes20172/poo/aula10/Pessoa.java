package br.ucsal.bes20172.poo.aula10;

public class Pessoa implements Comparable<Pessoa> {

	private String nome;

	private String endereco;

	private Integer anoNascimento;

	public Pessoa(String nome, String endereco, Integer anoNascimento) {
		super();
		this.nome = nome;
		this.endereco = endereco;
		this.anoNascimento = anoNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", endereco=" + endereco + ", anoNascimento=" + anoNascimento + "]";
	}

	// Considerar que anoNascimento � uma ordem "default", outros crit�rios de
	// ordena��o deveriam ser constru�dos fora da classe.
	@Override
	public int compareTo(Pessoa outraPessoa) {
		return anoNascimento.compareTo(outraPessoa.anoNascimento);
	}

	// @Override
	// public int compareTo(Pessoa outraPessoa) {
	// return anoNascimento > outraPessoa.anoNascimento ? 1 : anoNascimento <
	// outraPessoa.anoNascimento ? -1 : 0;
	// }

	// public void teste(int x) {
	// int y;
	// if (x > 0) {
	// y = 10;
	// } else {
	// y = 20;
	// }
	// // Operador tern�rio
	// y = x > 0 ? 10 : 20;
	// }

	// @Override
	// public int compareTo(Pessoa outraPessoa) {
	// if (anoNascimento > outraPessoa.anoNascimento) {
	// // retornar 1 significa que a pessoa atual � "maior" que a
	// // outraPessoa
	// return 1;
	// }
	// if (anoNascimento < outraPessoa.anoNascimento) {
	// // retornar -1 significa que a pessoa atual � "menor" que a
	// // outraPessoa
	// return -1;
	// }
	// // retornar 0 significa que a pessoa atual � "igual" que a outraPessoa
	// return 0;
	// }

}
