package br.ucsal.bes20172.poo.aula10;

import java.util.Comparator;

public class ComparadorPessoaPorNome implements Comparator<Pessoa>{

	@Override
	public int compare(Pessoa o1, Pessoa o2) {
		return o1.getNome().compareTo(o2.getNome());
	}

}
