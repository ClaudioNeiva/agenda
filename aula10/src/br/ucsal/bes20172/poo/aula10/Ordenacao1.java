package br.ucsal.bes20172.poo.aula10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Ordenacao1 {

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();

		nomes.add("claudio");
		nomes.add("antonio");
		nomes.add("maria");
		nomes.add("pedro");
		nomes.add("jo�o");

		exibirLista("Lista de nomes, como informado:", nomes);

		// Ordenar a lista de nomes (ordem crescente)
		Collections.sort(nomes);
		exibirLista("Lista de nomes, ordem crescente:", nomes);

		// Ordenar a lista de nomes (ordem decrescente)
		// Aten��o com o m�todo reverse, ele inverte a lista, desta forma, se vc
		// quiser a lista em ordem decrescente, primeiro vc deve garantir a
		// ordena��o utilizando o m�todo sort.
		Collections.sort(nomes);
		Collections.reverse(nomes);
		exibirLista("Lista de nomes, ordem decrescente:", nomes);

	}

	private static void exibirLista(String mensagem, List<String> lista) {
		System.out.println("\n" + mensagem);
		for (String item : lista) {
			System.out.println(item);
		}
	}

}
