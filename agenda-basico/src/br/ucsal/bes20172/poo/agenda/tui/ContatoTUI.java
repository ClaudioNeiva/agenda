package br.ucsal.bes20172.poo.agenda.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20172.poo.agenda.business.ContatoBO;
import br.ucsal.bes20172.poo.agenda.entity.Contato;

public class ContatoTUI {

	private static Scanner scanner = new Scanner(System.in);

	public static void cadastrar() {
		System.out.println("******* CADASTRAR CONTATO *******");

		System.out.println("Informe:");

		System.out.print("Nome:");
		String nome = scanner.nextLine();

		System.out.print("Telefone:");
		String telefone = scanner.nextLine();

		Contato contato = new Contato(nome, telefone);

		// FIXME Quando for implemento o uso de Exception no cadastro, o
		// tratamento da exce��o dever� ser implementado neste m�dodo.
		if (ContatoBO.cadastrar(contato)) {
			System.out.println("Cadastro realizado com suscesso.");
		} else {
			System.out.println("Erro ao realizar o cadastro.");
		}
	}

	public static void listar() {
		List<Contato> contatos = ContatoBO.obterTodos();
		exibirContatos(contatos);
	}

	public static void pesquisar() {
		System.out.println("******* PESQUISAR CONTATO *******");
		System.out.print("Informe o nome do contato:");
		String nome = scanner.nextLine();
		List<Contato> contatos = ContatoBO.pesquisar(nome);
		exibirContatos(contatos);
	}

	private static void exibirContatos(List<Contato> contatos) {
		if (contatos.isEmpty()) {
			System.out.println("Nenhum contato encontrado.");
		} else {
			System.out.println("Contatos:");
			for (Contato contato : contatos) {
				System.out.println(contato.getNome() + " - " + contato.getTelefone());
			}
		}
	}

}
