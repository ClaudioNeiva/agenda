package br.ucsal.bes20172.poo.agenda.tui;

import java.util.Scanner;

//TUI = Text User Interface
public class AgendaTUI {

	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		Integer opcao;
		
		do {
			exibirOpcoes();
			opcao = obterOpcaoDesejada();
			executar(opcao);
		}while(opcao != 9);
	}

	private static void exibirOpcoes() {
		// FIXME Utilizar enumera��es e/ou constantes para definir as op��es de menu.
		System.out.println("1 - Cadastrar");
		System.out.println("2 - Listar");
		System.out.println("3 - Pesquisar");
		System.out.println("9 - Sair");
	}
	
	private static Integer obterOpcaoDesejada() {
		System.out.println("Informe a op��o desejada:");
		Integer opcao = scanner.nextInt();
		scanner.nextLine();
		return opcao;
	}
	
	private static void executar(Integer opcao) {
		switch (opcao) {
		case 1:
			ContatoTUI.cadastrar();
			break;
		case 2:
			ContatoTUI.listar();
			break;
		case 3:
			ContatoTUI.pesquisar();
			break;
		case 9:
			System.out.println("Bye...");
			break;
		default:
			System.out.println("Op��o inv�lida!");
			break;
		}
	}
	
}
