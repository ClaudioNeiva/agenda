package br.ucsal.bes20172.poo.agenda.business;

import java.util.List;

import br.ucsal.bes20172.poo.agenda.entity.Contato;
import br.ucsal.bes20172.poo.agenda.persistence.ContatoDAO;

//BO = Business Object
public class ContatoBO {

	// FIXME � necess�rio utilizar Exceptions no lugar de retorno boolean para o
	// cadastro.
	public static boolean cadastrar(Contato contato) {
		if (!validar(contato)) {
			return false;
		}
		ContatoDAO.inserir(contato);
		return true;
	}

	// FIXME � necess�rio utilizar Exceptions no lugar de retorno boolean para a
	// valida��o.
	private static boolean validar(Contato contato) {
		if (contato.getNome().trim().isEmpty()) {
			return false;
		}
		if (contato.getTelefone().trim().length() < 8) {
			return false;
		}
		return true;
	}

	public static List<Contato> obterTodos() {
		return ContatoDAO.obterTodos();
	}

	public static List<Contato> pesquisar(String nome) {
		return ContatoDAO.pesquisar(nome);
	}

}
